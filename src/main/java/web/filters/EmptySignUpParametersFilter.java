package web.filters;

import javax.servlet.*;
import java.io.IOException;


public class EmptySignUpParametersFilter implements Filter {

    @Override
    public void init( FilterConfig filterConfig ) throws ServletException {

    }

    @Override
    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {

        if ( requestParametersEmpty( request ) ) {
            String forward = "signup.jsp";
            String errorMessage = "Proszę wypełnić wszystkie pola";
            request.setAttribute( "error_msg", errorMessage );
            request.getRequestDispatcher( forward ).forward( request, response );
        } else {
            chain.doFilter( request, response );
        }
    }

    private boolean requestParametersEmpty( ServletRequest request ) {

        String username = request.getParameter( "username" );
        String email = request.getParameter( "email" );
        String password = request.getParameter( "password" );
        String passwordConfirm = request.getParameter( "password_confirm" );

        return username.isEmpty()
                || email.isEmpty()
                || password.isEmpty()
                || passwordConfirm.isEmpty();
    }

    @Override
    public void destroy() {

    }
}