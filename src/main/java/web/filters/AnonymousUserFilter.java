package web.filters;

import domain.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class AnonymousUserFilter implements Filter {
    @Override
    public void init( FilterConfig filterConfig ) throws ServletException {

    }

    @Override
    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession();
        User user = (User) session.getAttribute( "user" );

        if ( user == null ) {
            response.getWriter().println("Access denied. You have to be logged in to access this site");
        } else {
            chain.doFilter( request, response );
        }
    }

    @Override
    public void destroy() {

    }
}