package web.filters;


import javax.servlet.*;
import java.io.IOException;


public class PasswordNotMatchingFilter implements Filter {

    @Override
    public void init( FilterConfig filterConfig ) throws ServletException {

    }

    @Override
    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {
        String password = request.getParameter( "password" );
        String passwordConfirm = request.getParameter( "password_confirm" );

        if ( !password.equals( passwordConfirm ) ) {
            String forward = "signup.jsp";
            String errorMessage = "Podane hasła różnią się";
            request.setAttribute( "error_msg", errorMessage );
            request.getRequestDispatcher( forward ).forward( request, response );
        } else {
            chain.doFilter( request, response );
        }
    }

    @Override
    public void destroy() {

    }
}