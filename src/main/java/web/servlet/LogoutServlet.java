package web.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet( "/logout" )
public class LogoutServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void init( ServletConfig config ) {

    }

    @Override
    protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        HttpSession session = request.getSession();
        session.invalidate();

        String forward = "/";
        request.getRequestDispatcher( forward ).forward( request, response );
    }
}