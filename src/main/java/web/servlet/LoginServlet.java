package web.servlet;

import domain.User;
import service.HsqlRepositoryFactory;
import service.UserRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet( "login" )
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private UserRepository userRepository;

    @Override
    public void init( ServletConfig config ) {
        userRepository = HsqlRepositoryFactory.getUserRepository();
    }

    @Override
    protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        String username = request.getParameter( "username" );
        String password = request.getParameter( "password" );

        User user = authenticate( username, password );

        if ( user != null ) {
            HttpSession session = request.getSession();
            session.setAttribute( "user", user );
            session.setAttribute( "username", user.getUsername() );

            String redirect = "/profile";
            response.sendRedirect( redirect );
        } else {
            String errorMessage = "Nieudana próba logowania";
            request.setAttribute( "error_msg", errorMessage );

            String forward = "/";
            request.getRequestDispatcher( forward ).forward( request, response );
        }
    }

    private User authenticate( String username, String password ) {
        User user = userRepository.getByUsername( username );

        if ( user != null && user.getPassword().equals( password ) ) {
            return user;
        }
        else {
            return null;
        }
    }
}