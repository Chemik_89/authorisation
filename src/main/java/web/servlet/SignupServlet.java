package web.servlet;

import domain.User;
import service.HsqlRepositoryFactory;
import service.UserRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet( "signup" )
public class SignupServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private UserRepository userRepository;

    @Override
    public void init( ServletConfig config ) {
        userRepository = HsqlRepositoryFactory.getUserRepository();
    }

    @Override
    protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        String username = request.getParameter( "username" );
        String email = request.getParameter( "email" );
        String password = request.getParameter( "password" );

        User user = new User( username, password, email );
        userRepository.persist( user );

        String forward = "index.jsp";
        request.getRequestDispatcher( forward ).forward( request, response );
    }
}