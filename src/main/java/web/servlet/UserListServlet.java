package web.servlet;

import domain.User;
import service.HsqlRepositoryFactory;
import service.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet( "/users" )
public class UserListServlet extends HttpServlet {

    private UserRepository userRepository;

    @Override
    public void init() throws ServletException {
        userRepository = HsqlRepositoryFactory.getUserRepository();
    }

    @Override
    protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        List< User> userList = userRepository.getAllUsers();
        request.setAttribute( "user_list", userList );

        String forward = "/WEB-INF/user_list.jsp";
        request.getRequestDispatcher( forward ).forward( request, response );
    }
}