package domain;

public abstract class DomainObject {

    protected int id;

    protected DomainObject () {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId( int id ) {
        this.id = id;
    }
}