package service;

import domain.User;

import java.util.List;

public interface UserRepository {

    void persist( User user );
    User getByUsername( String username );

    List<User> getAllUsers();
}