package service;

import domain.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DummyUserRepository implements UserRepository {

    private static List<User> usersList = new ArrayList<>();

    @Override
    public void persist( User user ) {
        if ( user.getId() < 0 ) {
            user.setId( usersList.size() );
            usersList.add( user );
        } else {
            usersList.set( user.getId(), user );
        }
    }

    @Override
    public User getByUsername( String username ) {

        Optional<User> userOptional = usersList.stream()
                .filter( user -> user.getUsername().equals( username ) )
                .findFirst();

        if ( userOptional.isPresent() ) {
            return  userOptional.get();
        } else {
            return null;
        }
    }

    @Override
    public List< User > getAllUsers() {
        return usersList;
    }

    public void reset() {
        usersList = new ArrayList<>();
    }
}