package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HsqlRepositoryFactory {
    public static HsqlUserRepository getUserRepository() {
        HsqlUserRepository repository = null;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
        } catch ( ClassNotFoundException e ) {
            e.printStackTrace();
        }
        try {
            String url = "jdbc:hsqldb:hsql://localhost/testdb";
            String username = "SA";
            String dbPassword = "";
            Connection connection = DriverManager.getConnection( url, username, dbPassword );
            repository = new HsqlUserRepository(connection);
        } catch ( SQLException e ) {
            e.printStackTrace();
        }

        return repository;
    }
}