package service;

import domain.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HsqlUserRepository implements UserRepository {

    private Connection connection;

    private PreparedStatement insertUser;
    private PreparedStatement selectUserByUsername;
    private PreparedStatement selectAllUsers;

    public HsqlUserRepository( Connection connection  ) {
        this.connection = connection;
        String insertSql = "INSERT INTO user ( username, email, password ) VALUES (?, ?, ?)";
        String selectUserByUsernameSql = "SELECT * FROM user WHERE username = ?";
        String selectAllUsersSql = "SELECT * FROM user";
        try {
            insertUser = this.connection.prepareStatement( insertSql, Statement.RETURN_GENERATED_KEYS );
            selectUserByUsername = this.connection.prepareStatement( selectUserByUsernameSql );
            selectAllUsers = this.connection.prepareStatement( selectAllUsersSql );
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
    }

    @Override
    public void persist( User user ) {
        try {
            insertUser.setString( 1, user.getUsername() );
            insertUser.setString( 2, user.getEmail() );
            insertUser.setString( 3, user.getPassword() );

            insertUser.executeUpdate(  );
            ResultSet resultSet = insertUser.getGeneratedKeys();

            if ( resultSet.next() ) {
                user.setId( resultSet.getInt( 1 ) );
            }
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
    }

    @Override
    public User getByUsername( String username ) {
        User user = null;
        try {
            selectUserByUsername.setString( 1, username );
            ResultSet resultSet = selectUserByUsername.executeQuery();
            resultSet.next();
            user = buildUser( resultSet );
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public List< User > getAllUsers() {
        List<User> userList = new ArrayList<>(  );
        try {
            ResultSet resultSet = selectAllUsers.executeQuery();
            resultSet.next();

            while( resultSet.next() ) {
                userList.add( buildUser( resultSet ) );
            }
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
        return userList;
    }

    private User buildUser( ResultSet resultSet ) {

        User user = null;
        try {
            String username = resultSet.getString( "username" );
            String email = resultSet.getString( "email" );
            String password = resultSet.getString( "password" );
            int id = resultSet.getInt( "ID" );

            user = new User(  );
            user.setId( id );
            user.setUsername( username );
            user.setEmail( email );
            user.setPassword( password );
        } catch ( SQLException e ) {
            e.printStackTrace();
        }

        return user;
    }
}