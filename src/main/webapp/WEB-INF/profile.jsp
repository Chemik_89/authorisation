<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<title>Fantastic website - User Profile</title>
</head>
<body>
    <div class="container">
        <div class="panel-heading" style="margin-top:50px;">
            <div style="float:right; position: relative; top:-10px">
                <a href="/users" class="btn btn-default" role="button">Users</a>
                <a href="/premium" class="btn btn-default" role="button">Premium</a>
                <a href="/logout" class="btn btn-default" role="button">Logout</a>
            </div>
        </div>
        <div class="col-md-12">
            <h1 class="text-center">To jest profil użytkownika:</h1>
            <h1 class="text-center">${sessionScope.user.getUsername()}</h1>
        </div>
    </div>
</body>
</html>
