<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <title>Fantastic website - User List</title>
</head>
<body>
<div class="container">
    <div class="panel-heading" style="margin-top:50px;">
        <div style="float:right; position: relative; top:-10px">
            <a href="/premium" class="btn btn-default" role="button">Premium</a>
            <a href="/logout" class="btn btn-default" role="button">Logout</a>
        </div>
    </div>
    <div class="col-md-12">
        <h1 class="text-center">Lista użytkowników:</h1>
        <table class="table table-striped table-condensed table-bordered">
            <thead>
            <tr>
                <th class="col-md-3">Username</th>
                <th class="col-md-3">E-mail</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="user" items="${user_list}">
                <tr>
                    <td><c:out value="${user.getUsername()}"/></td>
                    <td><c:out value="${user.getEmail()}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>