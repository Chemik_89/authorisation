package service;

import org.junit.AfterClass;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HsqlUserRepositoryTest extends UserRepositoryTest {

    private static Connection connection;
    private static String url = "jdbc:hsqldb:hsql://localhost/testdb";
    private static String username = "SA";
    private static String dbPassword = "";

    @Override
    protected UserRepository getRepositoryImplementation() {
        try {
            connection = DriverManager.getConnection( url, username, dbPassword );
        } catch ( SQLException e ) {
            e.printStackTrace();
        }

        return new HsqlUserRepository( connection );
    }

    @AfterClass
    public static void closeConnection() {
        try {
            connection.close();
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
    }

}