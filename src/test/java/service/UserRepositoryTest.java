package service;

import domain.User;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public abstract class UserRepositoryTest {
    protected UserRepository userRepository;

    public UserRepositoryTest() {
        userRepository = getRepositoryImplementation();
    }

    protected abstract UserRepository getRepositoryImplementation();

    @Test
    public void given_user_when_persistedInRepository_then_userHasIdAssigned() throws Exception {
        //Given
        User userSpy = spy( User.class );

        when( userSpy.getUsername() ).thenReturn( "admin" );
        when( userSpy.getEmail() ).thenReturn( "admin@admin.pl" );
        when( userSpy.getPassword() ).thenReturn( "admin" );

        //When
        userRepository.persist( userSpy );

        //Then
        assertThat( userSpy.getId() ).as( "User id" )
                .isGreaterThan( -1 );
    }

    @Test
    public void given_user_when_persist_then_userIsInRepository() throws Exception {
        // Given
        User user = Mockito.spy( User.class );
        String username = "testUsername";
        when( user.getUsername() ).thenReturn( username );
        when( user.getEmail() ).thenReturn( "test@mail.com" );
        when( user.getPassword() ).thenReturn( "pA$$w0rd" );

        // When
        userRepository.persist( user );
        User userFromRepo = userRepository.getByUsername( username );

        // Then
        assertThat( userFromRepo ).as( "User from repository" )
                .isNotNull()
                .isEqualToComparingFieldByField( user );
    }

    @Test
    public void given_userInRepository_when_getAllUsers_then_theUserIsOnList() throws Exception {
        //Given
        User karol = new User(  );
        karol.setUsername( "karol" );
        karol.setPassword( "lolek" );
        karol.setEmail( "lolek@karol.pl" );

        //When
        userRepository.persist( karol );
        List<User> userList = userRepository.getAllUsers();
        //Then
        assertThat( userList ).usingElementComparatorOnFields( "id", "username", "email", "password" );
    }
}