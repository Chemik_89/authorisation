package service;

import org.junit.Before;

public class DummyUserRepositoryTest extends UserRepositoryTest {

    @Override
    protected UserRepository getRepositoryImplementation() {
        return new DummyUserRepository();
    }

    @Before
    public void resetRepository() {
        DummyUserRepository repo = (DummyUserRepository) userRepository;
        repo.reset();
    }
}