package web.filters;

import org.junit.Test;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;

public class EmptySignUpParametersFilterTest {

    @Test
    public void given_requestWithEmptyParameters_when_signingUp_then_blockThisRequest() throws Exception {
        //Given
        HttpServletRequest request = mock( HttpServletRequest.class );
        when( request.getParameter( "username" ) ).thenReturn( "" );
        when( request.getParameter( "email" ) ).thenReturn( "" );
        when( request.getParameter( "password" ) ).thenReturn( "" );
        when( request.getParameter( "password_confirm" ) ).thenReturn( "" );

        RequestDispatcher dispatcher = mock( RequestDispatcher.class );
        when( request.getRequestDispatcher( any( String.class ) ) ).thenReturn( dispatcher );

        HttpServletResponse response = mock( HttpServletResponse.class );
        FilterChain chain = mock( FilterChain.class );

        //When
        EmptySignUpParametersFilter filter = new EmptySignUpParametersFilter();
        filter.doFilter( request, response, chain );

        //Then
        verify( chain, never() ).doFilter( any( HttpServletRequest.class ), any( HttpServletResponse.class ) );
    }
}