package web.filters;

import org.junit.Test;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class PasswordNotMatchingFilterTest {

    @Test
    public void given_notMatchingPasswords_when_accessingSignUpServlet_then_blockThisRequest() throws Exception {
        //Given
        HttpServletRequest request = mock( HttpServletRequest.class );
        when( request.getParameter( "password" ) ).thenReturn( "password" );
        when( request.getParameter( "password_confirm" ) ).thenReturn( "not_matching_password" );

        RequestDispatcher dispatcher = mock( RequestDispatcher.class );
        when( request.getRequestDispatcher( any( String.class ) ) ).thenReturn( dispatcher );

        HttpServletResponse response = mock( HttpServletResponse.class );
        FilterChain chain = mock( FilterChain.class );

        //When
        PasswordNotMatchingFilter filter = new PasswordNotMatchingFilter();
        filter.doFilter( request, response, chain );

        //Then
        verify( chain, never() ).doFilter( any( HttpServletRequest.class ), any( HttpServletResponse.class ) );
    }

}