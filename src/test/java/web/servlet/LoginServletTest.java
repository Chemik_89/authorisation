package web.servlet;

import domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import service.UserRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RunWith( MockitoJUnitRunner.class )
public class LoginServletTest extends Mockito {

    @Spy
    UserRepository repository = mock( UserRepository.class );

    @InjectMocks
    private LoginServlet loginServlet;

    @Test
    public void given_validLoginParameters_when_doPost_then_writeUserIntoSession() throws Exception {
        //Given
        String username = "admin";
        String password = "password";

        HttpServletRequest request = mock( HttpServletRequest.class );
        when( request.getParameter( "username" ) ).thenReturn( username );
        when( request.getParameter( "password" ) ).thenReturn( password );

        HttpSession session = mock( HttpSession.class );
        when( request.getSession() ).thenReturn( session );

        User user = mock( User.class );
        when( user.getPassword() ).thenReturn( password );
        when( repository.getByUsername( username ) ).thenReturn( user );

        HttpServletResponse response = mock( HttpServletResponse.class );

        //When
        loginServlet.doPost( request, response );

        //Then
        verify( session ).setAttribute( "user", user );
    }
}