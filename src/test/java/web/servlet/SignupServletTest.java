package web.servlet;

import domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import service.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RunWith( MockitoJUnitRunner.class )
public class SignupServletTest extends Mockito {

    @Spy
    UserRepository repository = Mockito.mock( UserRepository.class );

    @InjectMocks
    private SignupServlet signupServlet;

    @Test
    public void given_httpRequest_when_doPost_then_persistUserInRepository() throws Exception {
        // Given
        HttpServletResponse response = Mockito.mock( HttpServletResponse.class );
        HttpServletRequest request = Mockito.mock( HttpServletRequest.class );
        RequestDispatcher dispatcher = Mockito.mock( RequestDispatcher.class );
        when( request.getRequestDispatcher( any( String.class ) ) ).thenReturn( dispatcher );

        // When
        signupServlet.doPost( request, response );

        // Then
        verify( repository ).persist( Mockito.any( User.class ) );
    }

    @Test
    public void given_signUpServlet_when_doPost_then_forwardToLoginPage() throws Exception {
        //Given
        HttpServletResponse response = Mockito.mock( HttpServletResponse.class );
        HttpServletRequest request = Mockito.mock( HttpServletRequest.class );
        RequestDispatcher dispatcher = Mockito.mock( RequestDispatcher.class );
        when( request.getRequestDispatcher( any( String.class ) ) ).thenReturn( dispatcher );

        //When
        signupServlet.doPost( request, response );

        //Then
        verify( request ).getRequestDispatcher( "index.jsp" );
        verify( dispatcher ).forward( any( HttpServletRequest.class ), any( HttpServletResponse.class )  );
    }
}